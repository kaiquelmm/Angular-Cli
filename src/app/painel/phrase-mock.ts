import { Phrase } from '../shared/phrase.model';

export const PHRASE: Phrase[] = [
  {phraseEng: 'I like to learn' , phrasePtBr: 'Eu gosto de aprender'},
  {phraseEng: 'I watch TV' , phrasePtBr: 'Eu assisto TV'},
  {phraseEng: 'How Are You' , phrasePtBr: 'Como você esta?'},
  {phraseEng: 'I eat bread' , phrasePtBr: 'Eu como pão'},
];
