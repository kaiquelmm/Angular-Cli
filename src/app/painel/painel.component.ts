import { Component, OnInit } from '@angular/core';
import { Phrase } from '../shared/phrase.model';
import { PHRASE } from './phrase-mock';

@Component({
  selector: 'app-painel',
  templateUrl: './painel.component.html',
  styleUrls: ['./painel.component.css']
})
export class PainelComponent implements OnInit {

  public phrases: Phrase[] = PHRASE;
  public instruction = 'Traduza a frase:';
  public answerQuestion: string;
  public round = 0;
  public roundPhrase: Phrase;

  constructor() {
  this.roundPhrase = this.phrases[this.round];
    console.log(this.roundPhrase);
  }

  ngOnInit() {
  }
  public updateAnswer(answer: Event): void {
    this.answerQuestion = ((<HTMLInputElement>answer.target).value);
    // console.log(this.answerQuestion);
  }

  public verifyAnswer(): void {
    if (this.roundPhrase.phrasePtBr === this.answerQuestion) {
      alert('A tradução esta certa!');
      this.round++;
      this.roundPhrase = this.phrases[this.round];

    } else {
      alert('A tradução esta errada!');
    }
  }
}
